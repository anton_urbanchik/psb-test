package psb.psbtest.controllers;

import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import psb.psbtest.models.Person;
import psb.psbtest.service.PersonService;

import java.util.List;

@RestController
@AllArgsConstructor
public class PersonController {

    private final PersonService personService;

    @GetMapping(path = "/contacts", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    List<Person> getAllContacts() {
        return personService.getAllContacts();
    }

}
