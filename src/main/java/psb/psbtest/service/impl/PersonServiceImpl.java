package psb.psbtest.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import psb.psbtest.models.Person;
import psb.psbtest.repositories.PersonRepository;
import psb.psbtest.service.PersonService;

import java.util.List;

@RequiredArgsConstructor
@Service
public class PersonServiceImpl implements PersonService {

    public final PersonRepository personRepository;

    @Override
    public List<Person> getAllContacts() {
        List<Person> personList = personRepository.findPersonByParent_IdIsNullOrderByName();
        personList.forEach(x -> x.getContactList().sort(Person::compareTo));
        return personList;
    }
}
