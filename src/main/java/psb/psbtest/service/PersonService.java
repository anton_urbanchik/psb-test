package psb.psbtest.service;

import psb.psbtest.models.Person;

import java.util.List;

public interface PersonService {
    List<Person> getAllContacts();
}
