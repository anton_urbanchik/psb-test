package psb.psbtest.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import psb.psbtest.models.Person;

import java.util.List;

public interface PersonRepository extends JpaRepository<Person, Integer> {
    List<Person> findPersonByParent_IdIsNullOrderByName();
}
