package psb.psbtest.forms;

import lombok.Data;

@Data
public class PersonForm {
    private String name;
}
