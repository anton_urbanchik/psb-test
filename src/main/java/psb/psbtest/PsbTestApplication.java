package psb.psbtest;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import psb.psbtest.service.PersonService;

@SpringBootApplication
@RequiredArgsConstructor
public class PsbTestApplication {

    public final PersonService personService;

    public static void main(String[] args) {
        SpringApplication.run(PsbTestApplication.class, args);


    }

}
